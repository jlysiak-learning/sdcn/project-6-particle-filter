/**
 * particle_filter.h
 * 2D particle filter class.
 *
 * Created on: Dec 12, 2016
 * Author: Tiffany Huang, Jacek Lysiak
 */

#ifndef PARTICLE_FILTER_H_
#define PARTICLE_FILTER_H_

#include "helper_functions.h"
#include "particle.h"
#include "map_observation.h"
#include "landmark_obs.h"

#include <cassert>
#include <random>
#include <string>
#include <vector>


class ParticleFilter {
public:
  ParticleFilter() : particles_n(0), is_initialized(false) {}

  /**
   * init Initializes particle filter by initializing particles to Gaussian
   *   distribution around first position and all the weights to 1.
   * @param n, Number of particles to draw
   * @param x Initial x position [m] (simulated estimate from GPS)
   * @param y Initial y position [m]
   * @param theta Initial orientation [rad]
   * @param std[] 1D array of size 3 [standard deviation of x [m],
   *   standard deviation of y [m], standard deviation of yaw [rad]]
   */
  void init(int n, double x, double y, double theta, double const std[3]);

  /**
   * prediction Predicts the state for the next time step
   *   using the process model.
   * @param delta_t Time between time step t and t+1 in measurements [s]
   * @param std_pos[] 1D array of size 3 [standard deviation of x [m],
   *   standard deviation of y [m], standard deviation of yaw [rad]]
   * @param velocity Velocity of car from t to t+1 [m/s]
   * @param yaw_rate Yaw rate of car from t to t+1 [rad/s]
   */
  void prediction(double delta_t, double const std_pos[3], double velocity,
                  double yaw_rate);

  /**
   * updateWeights Updates the weights for each particle based on the likelihood
   *   of the observed measurements.
   * @param sensor_range Range [m] of sensor
   * @param std_landmark[] Array of size 2
   *   [Landmark measurement uncertainty [x [m], y [m]]]
   * @param observations Vector of landmark observations
   * @param map Map class containing map landmarks
   */
  void updateWeights(double sensor_range, double const std_landmark[2],
                     const std::vector<LandmarkObs> &observations,
                     const Map &map_landmarks);

  /**
   * resample Resamples from the updated set of particles to form
   *   the new set of particles.
   */
  void resample();

  /**
   * initialized Returns whether particle filter is initialized yet or not.
   */
  const bool initialized() const { return is_initialized; }

  /**
   * Used for obtaining debugging information related to particles.
   */
  std::string getAssociations(Particle const  &best);
  std::string getSenseCoord(Particle const &best, std::string coord);

  std::vector<Particle> const & getParticles() const { return particles; }

private:
  /**
   * dataAssociation Finds which observations correspond to which landmarks
   *   (likely by using a nearest-neighbors data association).
   * @param map_coords_observations vector of measurements transformed to map
   *    coordinate system
   * @param landmarks_in_range vector of landmarks that are in sensor range
   */
  void dataAssociation(
                std::vector<std::pair<double, double> > &residuals_squared,
                std::vector<MapObs> &map_coords_observations,
                std::vector<Landmark> const &landmarks_in_range);


private:
  int particles_n;

  // Flag, if filter is initialized
  bool is_initialized;

  // Set of current particles
  std::vector<Particle> particles;

  std::default_random_engine rng;
};

#endif // PARTICLE_FILTER_H_
