#ifndef PARTICLE_H
#define PARTICLE_H

#include <vector>

class Particle {
public:
  Particle(double x, double y, double theta);
  Particle(double x, double y, double theta, double weight);

  void move(double dt, double vel, double yaw_rate, double pos_noise[3]);

public:
  double x;
  double y;
  double theta;
  double sin_theta;
  double cos_theta;
  double weight;
  int id;

  std::vector<int> associations;
  std::vector<double> sense_x;
  std::vector<double> sense_y;

private:
  static int next_id;
  static int get_next_particle_id();
};


#endif
