#include "map.h"

#include <fstream>
#include <sstream>

Landmark::Landmark(double x, double y, int id) : x(x), y(y), id(id) {}

/** Reads map data from a file.
 * @param filename Name of file containing map data.
 * @output True if opening and reading file was successful
 */
bool Map::read_map_data(std::string filename, Map &map) {
  // Get file of map
  std::ifstream in_file_map(filename.c_str(), std::ifstream::in);
  // Return if we can't open the file
  if (!in_file_map) {
    return false;
  }

  // Declare single line of map file
  std::string line_map;

  // Run over each single line
  while (getline(in_file_map, line_map)) {

    std::istringstream iss_map(line_map);

    // Declare landmark values and ID
    double x, y;
    int id;

    // Read data from current line to values
    iss_map >> x >> y >> id;
    map.landmarks.push_back(Landmark(x, y, id));
  }
  return true;
}
