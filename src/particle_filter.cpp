/**
 * Particle filer impl.
 * Author: Jacek Lysiak
 */

#include "particle_filter.h"
#include "helper_functions.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <cmath>
#include <numeric>
#include <string>
#include <vector>
#include <utility>


// Squared distance, do not waste time for computing SQRT
static double distance2(Particle const &p, Landmark const &l) {
    double dx = p.x - l.x;
    double dy = p.y - l.y;
    return dx * dx + dy * dy;
}

void ParticleFilter::init(int n, double x, double y, double theta, double const std[3]) {
  assert(n > 0);
  particles_n = n;

  particles.clear();

  std::normal_distribution<double> noise_x(0., std[0]);
  std::normal_distribution<double> noise_y(0., std[1]);
  std::normal_distribution<double> noise_theta(0., std[2]);

  for (auto i = 0; i < particles_n; i++) {
      auto x_ = x + noise_x(rng);
      auto y_ = y + noise_y(rng);
      auto theta_ = theta + noise_theta(rng);
      particles.push_back(Particle(x_, y_, theta_));
  }

  is_initialized = true;
}

void ParticleFilter::prediction(double delta_t, double const std_pos[3],
                                double velocity, double yaw_rate) {
  std::normal_distribution<double> noise_x(0., std_pos[0]);
  std::normal_distribution<double> noise_y(0., std_pos[1]);
  std::normal_distribution<double> noise_theta(0., std_pos[2]);
  for (auto &p : particles) {
      double pos_noise[3];
      pos_noise[0] = noise_x(rng);
      pos_noise[1] = noise_y(rng);
      pos_noise[2] = noise_theta(rng);
      p.move(delta_t, velocity, yaw_rate, pos_noise);
  }
}

void ParticleFilter::dataAssociation(
        std::vector<std::pair<double, double> > &residuals_squared,
        std::vector<MapObs> &map_coords_observations,
        std::vector<Landmark> const &landmarks_in_range) {
    // Find <obs, landmark> association using nearest neighbor
    // We assigns a landmark to EACH observation.
    for (auto &map_obs : map_coords_observations) {
        // do not take sqrt, no sense in doing that
        double min_dist2 = std::numeric_limits<double>::infinity();
        double min_dx2;
        double min_dy2;
        for (auto const &landmark : landmarks_in_range) {
            double dx = (map_obs.x - landmark.x);
            double dx2 = dx * dx;
            double dy = (map_obs.y - landmark.y);
            double dy2 = dy * dy;
            double dist2 = dx2 + dy2;
            if (dist2 < min_dist2) {
                min_dist2 = dist2;
                map_obs.id = landmark.id;
                min_dx2 = dx2;
                min_dy2 = dy2;
            }
        }
        residuals_squared.push_back(std::make_pair(min_dx2, min_dy2));
    }
}

void ParticleFilter::updateWeights(double sensor_range, double const std_landmark[2],
                                   const std::vector<LandmarkObs> &observations,
                                   const Map &map_landmarks) {
  /**
   * Update the weights of each particle using a mult-variate Gaussian
   *   distribution. You can read more about this distribution here:
   *   https://en.wikipedia.org/wiki/Multivariate_normal_distribution
   * NOTE: The observations are given in the VEHICLE'S coordinate system.
   *   Your particles are located according to the MAP'S coordinate system.
   *   You will need to transform between the two systems. Keep in mind that
   *   this transformation requires both rotation AND translation (but no
   * scaling). The following is a good resource for the theory:
   *   https://www.willamette.edu/~gorr/classes/GeneralGraphics/Transforms/transforms2d.htm
   *   and the following is a good resource for the actual equation to implement
   *   (look at equation 3.33) http://planning.cs.uiuc.edu/node99.html
   */
    // Constants used later, when computing new weights.
    const double pi = 2 * std::acos(0);
    // PDF normalizer
	double pdf_norm = 2 * pi * std_landmark[0] * std_landmark[1];
    // X var exponent normalizer
	double exp_x_norm = 2 * std_landmark[0] * std_landmark[0];
    // Y var exponent normalizer
	double exp_y_norm = 2 * std_landmark[1] * std_landmark[1];
    // Range squared for distance comparisons 
    const double range2 = sensor_range * sensor_range;
    for (auto &p : particles) {
        // Convert observations from car's coordinate system to map coordinate system
        std::vector<MapObs> transformed_observations;
        for (auto const &obs_car: observations) {
            transformed_observations.push_back(MapObs(obs_car, p));
        }

        std::vector<Landmark> visible_landmarks;
        for (auto const &landmark : map_landmarks.landmarks) {
            if (distance2(p, landmark) > range2) {
                // ignore this landmark, could not be detected
                continue;
            }
            visible_landmarks.push_back(landmark);
        }
        // Associate transformed observation with visible landmarks
        // To save computation time, store residuals squared (for x and y
        // separately to use them when computing new weights)
        std::vector<std::pair<double, double> > residuals_squared;
        dataAssociation(residuals_squared,
                        transformed_observations,
                        visible_landmarks);
        p.weight = 1.;
        for (auto const &d2 : residuals_squared) {
            double ex = d2.first / exp_x_norm;
            double ey = d2.second / exp_y_norm;
            p.weight *= std::exp(-(ex + ey)) / pdf_norm;
        }
        // Copy data for debugging in the sim
        p.associations.clear();
        p.sense_x.clear();
        p.sense_y.clear();
        for (auto const &map_obs : transformed_observations) {
            p.associations.push_back(map_obs.id);
            p.sense_x.push_back(map_obs.x);
            p.sense_y.push_back(map_obs.y);
        }
    }
}

void ParticleFilter::resample() {
    std::vector<Particle> new_particles;
    std::vector<double> weights;
    for (auto const &p : particles) {
        weights.push_back(p.weight);
    }
    std::discrete_distribution<int> dist(weights.begin(), weights.end());
    for (size_t i = 0; i < particles.size(); ++i) {
        int idx = dist(rng);
        new_particles.push_back(particles[idx]);
    }
    particles = new_particles;
}

std::string ParticleFilter::getAssociations(Particle const &best) {
    std::vector<int> v = best.associations;
  std::stringstream ss;
  copy(v.begin(), v.end(), std::ostream_iterator<int>(ss, " "));
  std::string s = ss.str();
  s = s.substr(0, s.length() - 1); // get rid of the trailing space
  return s;
}

std::string ParticleFilter::getSenseCoord(Particle const &best, std::string coord) {
    std::vector<double> v;

  if (coord == "X") {
    v = best.sense_x;
  } else {
    v = best.sense_y;
  }

  std::stringstream ss;
  copy(v.begin(), v.end(), std::ostream_iterator<float>(ss, " "));
  std::string s = ss.str();
  s = s.substr(0, s.length() - 1); // get rid of the trailing space
  return s;
}
