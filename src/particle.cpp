#include "particle.h"

#include <cmath>

Particle::Particle(double x, double y, double theta) : x(x), y(y), theta(theta) {
    id = get_next_particle_id();
    weight = 1.;
    sin_theta = std::sin(theta);
    cos_theta = std::cos(theta);
}

Particle::Particle(double x, double y, double theta, double weight) :
        x(x), y(y), theta(theta), weight(weight) {
    id = get_next_particle_id();
    sin_theta = std::sin(theta);
    cos_theta = std::cos(theta);
}

double def_noise[3] = {0., 0., 0.};
void Particle::move(double dt, double vel, double yaw_rate, double pos_noise[3]=def_noise) {
    const double eps = 0.0001;
    double theta1 = theta + yaw_rate * dt;
    double dx;
    double dy;

    if (yaw_rate < eps) {
        dx = vel * dt * cos_theta;
        dy = vel * dt * sin_theta;
    } else {
        double a = vel / yaw_rate;
        dx = a * (std::sin(theta1) - sin_theta);
        dy = a * (-std::cos(theta1) + cos_theta);
    }

    x = x + dx + pos_noise[0];
    y = y + dy + pos_noise[1];
    theta = theta1 + pos_noise[2];
    sin_theta = std::sin(theta);
    cos_theta = std::cos(theta);
}

int Particle::next_id = 0;

int Particle::get_next_particle_id() {
    return Particle::next_id++;
}
