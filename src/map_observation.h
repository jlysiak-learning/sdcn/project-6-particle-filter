#ifndef MAP_OBSERVATION_H
#define MAP_OBSERVATION_H

#include "landmark_obs.h"
#include "particle.h"

class MapObs {
public:
    MapObs(LandmarkObs const & obs, Particle const & particle);

    int id;   // associated landmark
    double x; // pos x in map coorods
    double y; // pos y in map coords
};

#endif
