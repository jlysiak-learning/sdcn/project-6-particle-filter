#include "map_observation.h"
#include "particle.h"
#include "landmark_obs.h"

MapObs::MapObs(LandmarkObs const & obs, Particle const & particle) {
    x = particle.x + obs.x * particle.cos_theta - obs.y * particle.sin_theta;
	y = particle.y + obs.x * particle.sin_theta + obs.y * particle.cos_theta;
    id = -1; // not associated
}
