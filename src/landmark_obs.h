#ifndef LANDMARK_OBS_H
#define LANDMARK_OBS_H

/**
 * Struct representing one landmark observation measurement.
 */
struct LandmarkObs {

  int id;   // Id of matching landmark in the map.
  double x; // Local (vehicle coords) x position of landmark observation [m]
  double y; // Local (vehicle coords) y position of landmark observation [m]
};

#endif
