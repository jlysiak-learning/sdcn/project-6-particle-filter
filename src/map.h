#ifndef MAP_H_
#define MAP_H_

#include <string>
#include <vector>

class Landmark {
public:
  Landmark(double x, double y, int id);

public:
  double x; // Landmark x-position in the map (global coordinates)
  double y; // Landmark y-position in the map (global coordinates)
  int id;  // Landmark ID
};

class Map {
public:
  std::vector<Landmark> landmarks;

public:
  static bool read_map_data(std::string filename, Map &map);
};

#endif // MAP_H_
