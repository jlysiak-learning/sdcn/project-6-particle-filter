# Self-Driving Car Nanodegree project: Particle Filter

Starter code for this project is
[here](https://github.com/udacity/CarND-Kidnapped-Vehicle-Project).

Term2 simulator can be found [here](https://github.com/udacity/self-driving-car-sim/releases).

## Directory contents

- `src`: particle filter code
- `data`: landmarks data file
- `install-linux.sh`: installs required tools, like cmake, gcc, libssl, etc.
  Also, downloads `uWebSockets` from github, builds it and copies important
  files: `libuWS.so` and include files.
- `build.sh`: just runs cmake and make
- `clean.sh`: ...
- `run.sh`: runs the app, it's important to set CWD to repo's root dir because
  of hardcoded (yes...) data file path.

## Building the app

1. Run `./install.sh` to install deps
2. Run `./build.sh`
3. Run Term2 sim and select Kidnapped Vehicle project
4. Run `./run.sh`

## Results

Filter uses 50 particles.
I didn't like the original structure, so I created more classes to separate the
logic and make things clear to the readers.

Result are shown below:
![img](./images/success.png)
